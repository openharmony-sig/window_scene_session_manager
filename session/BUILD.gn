# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/window/scene_session_manager/scenesessionmanager_aafwk.gni")

config("session_public_config") {
  include_dirs = [
    "${session_base_path}",
    "${session_base_path}/intention_event/service/anr_manager/include",
    "${session_base_path}/intention_event/framework/anr_handler/include",
    "${session_base_path}/intention_event/utils/include",
    "${window_base_path}/interfaces/innerkits/dm",

    # for window_manager_hilog
    "${window_base_path}/utils/include",
    "${session_base_path}/common/include",

    # for WMError Code
    "${window_base_path}/interfaces/innerkits/wm",

    # for mouse style
    "${multimodalinput_path}/input/util/common/include",
  ]
}

ohos_shared_library("scene_session") {
  sources = [
    "container/src/window_event_channel.cpp",
    "container/src/zidl/session_stage_proxy.cpp",
    "container/src/zidl/session_stage_stub.cpp",
    "container/src/zidl/window_event_channel_proxy.cpp",
    "container/src/zidl/window_event_channel_stub.cpp",
    "host/src/extension_session.cpp",
    "host/src/move_drag_controller.cpp",
    "host/src/root_scene_session.cpp",
    "host/src/scene_persistence.cpp",
    "host/src/scene_persistent_storage.cpp",
    "host/src/scene_session.cpp",
    "host/src/session.cpp",
    "host/src/zidl/session_proxy.cpp",
    "host/src/zidl/session_stub.cpp",
  ]

  public_configs = [ ":session_public_config" ]

  deps = [
    "${session_base_path}/common:window_scene_common",
    "${session_base_path}/intention_event/service:intention_event_anr_manager",
    "${session_base_path}/session_manager:screen_session_manager",
  ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "ability_runtime:ability_start_setting",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "graphic_2d:librender_service_client",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "init:libbegetutil",
    "input:libmmi-client",
    "ipc:ipc_single",
    "preferences:native_preferences",
    "window_manager:libwmutil",
    "window_manager:libwsutils",
  ]

  defines = []
  if (defined(global_parts_info) &&
      defined(global_parts_info.powermgr_power_manager)) {
    external_deps += [ "power_manager:powermgr_client" ]
    defines += [ "POWER_MANAGER_ENABLE" ]
  }

  innerapi_tags = [ "platformsdk" ]
  part_name = "scene_session_manager"
  subsystem_name = "window"
}

ohos_shared_library("screen_session") {
  sources = [
    "screen/src/screen_property.cpp",
    "screen/src/screen_session.cpp",
  ]

  public_configs = [ ":session_public_config" ]

  deps = []

  external_deps = [
    "c_utils:utils",
    "graphic_2d:librender_service_client",
    "hilog:libhilog",
    "init:libbegetutil",
    "window_manager:libwmutil",
  ]

  innerapi_tags = [ "platformsdk" ]
  part_name = "scene_session_manager"
  subsystem_name = "window"
}
