# scene_session_manager

- [scene\_session\_manager](#scene_session_manager)
  - [简介](#简介)
  - [目录](#目录)
  - [约束](#约束)
  - [相关仓](#相关仓)

## 简介

**场景会话管理子系统** 提供场景会话管理系统，对接window_manager


## 目录
```
foundation/window/scene_session_manager/
├─common                                 # 通用层
│  ├─include
│  └─src
├─intention_event                        # 内部事件
│  ├─dfx
│  ├─framework
│  ├─include
│  ├─service
│  ├─src
│  └─utils
├─interfaces                             # 对外接口
│  ├─include
│  ├─innerkits
│  └─kits
├─session                                # 会话
│  ├─container
│  ├─host
│  └─screen
├─session_manager                        # 会话管理
│  ├─include
│  └─src
├─session_manager_service                # 会话管理服务
│  ├─include
│  └─src
└─test                                   # 测试框架相关
    ├─mock
    └─unittest
```

## 约束
- 语言版本
    - C++11或以上

## 相关仓
- [window_manager](https://gitee.com/openharmony/window_window_manager)