# Scene Session Manager

- [Scene Session Manager](#scene-session-manager)
  - [Introduction](#introduction)
  - [Directory](#directory)
  - [Constraints](#constraints)
  - [Repositories Involved](#repositories-involved)

## Introduction

**scene session manager** provide session manager service, which works with window_manager.


## Directory
```
foundation/window/scene_session_manager/
├─common                                 # common layer
│  ├─include
│  └─src
├─intention_event                        # intention event
│  ├─dfx
│  ├─framework
│  ├─include
│  ├─service
│  ├─src
│  └─utils
├─interfaces                             # interface
│  ├─include
│  ├─innerkits
│  └─kits
├─session                                # session
│  ├─container
│  ├─host
│  └─screen
├─session_manager                        # session manager
│  ├─include
│  └─src
├─session_manager_service                # session manager service
│  ├─include
│  └─src
└─test                                   # 测试框架相关
    ├─mock
    └─unittest
```

## Constraints
- Programming language version
  - C++ 11 or later


## Repositories Involved
- [window_manager](https://gitee.com/openharmony/window_window_manager)